# E-Commerce Boilerplate v2

Newly created e-commerce boilerplate v2 dated 02 Oct 2015.

Functions included:
- News and blogs
- Gallery
- Newsletter with Mailchimp
- Quick checkout (a.k.a. One page checkout)
- Power Image Manager
- CK Editor with additional plugins (e.g. Google Map)
- Master slider
- Formula-based shipping
- FAQ management
- eNets payment method (cc and dc)
- Zopim live chat
- Access control for admin panel
