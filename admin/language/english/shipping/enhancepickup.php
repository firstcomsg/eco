<?php
// Heading
$_['heading_title']    = 'Pickup From Store (Multiple Locations)';

// Text 
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified pickup from store!';

// Entry
$_['entry_geo_zone']   = 'Geo Zone:';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sort Order:';
$_['entry_title'] = 'Title:';
$_['entry_desc'] = 'Additional Information(This information will display where the shipping options are showing on customer end):';
$_['button_add_store'] = 'Add store location';

$_['entry_total']      = 'Total:<br /><i class="help">Sub-Total amount needed before this location becomes available.</i>';
$_['entry_cost']       = 'Cost:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify pickup from store!';
?>