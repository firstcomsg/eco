<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
<script type="text/javascript">   
	function setEqualHeight(columns) { 
		var tallestcolumn = 0;
 			columns.each( function() {
 				currentHeight = $(this).height();
 				if(currentHeight > tallestcolumn)  {
 					tallestcolumn  = currentHeight; } 
 				});
 				columns.height(tallestcolumn);  }
	$(document).ready(function() {
 		setEqualHeight($(".gallheight<?php echo $gallimage_id; ?>"));
	});	
</script>	 
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <?php if ($thumb || $description) { ?>
      <div class="row">
        <?php if ($thumb) { ?>
        <div class="col-sm-2"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail" /></div>
        <?php } ?>
        <?php if ($description) { ?>
        <div class="col-sm-10"><?php echo $description; ?></div>
        <?php } ?>
      </div>
      <hr>
      <?php } ?>
      <div class="row">
  <?php foreach ($gallimages as $gallimage) { ?>
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
  <div class="box-gallery transition">      
      <div class="image" style="max-height:<?php echo $gheight; ?>px;"><a href="<?php echo $gallimage['popup']; ?>" title="<?php echo $gallimage['title']; ?>" class="gallbox"><img src="<?php echo $gallimage['image']; ?>" alt="<?php echo $gallimage['title']; ?>" title="<?php echo $gallimage['title']; ?>" class="img-responsive" /></a></div>
      <div class="caption gallheight<?php echo $gallimage_id; ?>">
  <?php if ($gallimage['link']) { ?>
  <h4><a href="<?php echo $gallimage['link']; ?>"><?php echo $gallimage['title']; ?></a></h4>
  <?php } else { ?>
  <h4><?php echo $gallimage['title']; ?></h4>
  <?php } ?>
  </div>
  </div>
  </div>
  <?php } ?>
  </div>    
  <?php echo $content_bottom; ?>    
  </div>
  <?php echo $column_right; ?></div>
</div>
<style type="text/css">
.box-gallery { text-align:center; }
</style>
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.gallbox').gallbox({
		overlayClose: true,
		opacity: 0.5,
		rel: "gallbox",
		maxWidth:<?php echo $pwidth; ?>, maxHeight:<?php echo $pheight; ?>, width:"96%"
	});
});
//--></script> 
<?php echo $footer; ?> 