<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
      </div>
      <div class="panel-body">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">
        <table class="form">
		<tr>
            <td colspan="2" id="button_add_store">
			<?php for($i=0;$i<count($enhancepickup_title);$i++){?>
			<div><table border="0">
                <tr>
                  <td><?php echo $entry_title; ?></td>
                  <td><input type="text" name="enhancepickup_title[]" value="<?php echo $enhancepickup_title[$i]; ?>" class="form-control" /></td>
                  <td width="150"><?php echo $entry_desc; ?></td>
                  <td><textarea name="enhancepickup_desc[]" rows="5" cols="50" class="form-control"><?php echo $enhancepickup_desc[$i]; ?></textarea></td>
                </tr>
				<tr>
				<td colspan="4">
				<table>
				<tr><td width="25%"><?php echo $entry_total; ?></td><td><input type="text" name="enhancepickup_total[]" value="<?php echo $enhancepickup_total[$i]; ?>"  class="form-control"/></td></tr>
				<tr><td width="25%"><?php echo $entry_cost; ?></td><td><input type="text" name="enhancepickup_cost[]" value="<?php echo $enhancepickup_cost[$i]; ?>"  class="form-control"/></td></tr>
				<tr>
				  <td><?php echo $entry_geo_zone; ?></td>
				  <td><select name="enhancepickup_geo_zone_id[]"  class="form-control">
                <option value="0"><?php echo $text_all_zones; ?></option>
                <?php foreach ($geo_zones as $geo_zone) { ?>
                <?php if ($geo_zone['geo_zone_id'] == $enhancepickup_geo_zone_id[$i]) { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
				  </tr>
				</table>
				</td>
				</tr>
				
              </table><a onclick="removeloc(this)" class="btn btn-default">Delete</a></div>
			  <?php }?>
			  <div id="loc"></div></td>
          </tr>
		  <tr>
            <td>&nbsp;</td>
            <td><a onclick="addModule();" class="btn btn-default"><span><?php echo $button_add_store; ?></span></a></td>
          </tr>
		  
        
          <tr>
            <td><?php echo $entry_status; ?></td>
            <td><select name="enhancepickup_status"  class="form-control">
                <?php if ($enhancepickup_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_sort_order; ?></td>
            <td><input type="text" name="enhancepickup_sort_order" value="<?php echo $enhancepickup_sort_order; ?>" size="1" class="form-control" /></td>
          </tr>
        </table>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
var module_row = <?php echo $module_row; ?>;

function addModule() {	
	html = '<div><table border="0"><tr><td><?php echo $entry_title; ?></td><td><input type="text" name="enhancepickup_title[]" class="form-control" /></td><td width="150"><?php echo $entry_desc; ?></td><td><textarea name="enhancepickup_desc[]" rows="5" cols="50" class="form-control"></textarea></td></tr><tr><td colspan="4"><table><tr><td width="25%"><?php echo $entry_total; ?></td><td><input type="text" name="enhancepickup_total[]" value="0" class="form-control" /></td></tr><tr><td width="25%"><?php echo $entry_cost; ?></td><td><input type="text" name="enhancepickup_cost[]" value="0"  class="form-control"/></td></tr><tr><td><?php echo $entry_geo_zone; ?></td><td><select name="enhancepickup_geo_zone_id[]" class="form-control"><option value="0"><?php echo $text_all_zones; ?></option><?php foreach ($geo_zones as $geo_zone) { ?><option value="<?php echo $geo_zone["geo_zone_id"]; ?>"><?php echo $geo_zone["name"]; ?></option><?php } ?></select></td></tr></table></td></tr></table><a onclick="removeloc(this)" class="btn btn-default">Delete</a></div>';
	$('#button_add_store div#loc').before(html);
	module_row++;
}

function removeloc(obj){
	$(obj).parent().remove();
}
//--></script>
<style>
td { 
    padding: 5px;
}
</style>
<?php echo $footer; ?>